import React, { Component } from "react";
import { render } from "react-dom";

const token = "	1d8abbadd50b9caa2955e7dc5c95aff9d9e64a76"
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    fetch('/api/patient/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': `Token ${token}`
        // 'Content-Type': 'application/json'
      }
    })
      .then(res => {
        if (res.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" }
          });
        }
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }


  render() {
    return (
      <h1>hELLO</h1>
    );
  }
}

export default App;

const container = document.getElementById("app");
render(<App />, container);