from django.contrib import admin
from . import models

admin.site.register(models.Patient)
admin.site.register(models.Equipment)
admin.site.register(models.Sensor)
admin.site.register(models.SensorType)
admin.site.register(models.UseHistory)
admin.site.register(models.Collect)
admin.site.register(models.Sample)
