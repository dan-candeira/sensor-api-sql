from rest_framework.routers import DefaultRouter
from django.urls import path, include
from . import views


app_name = 'api'
router = DefaultRouter()

# patient urls
router.register('patient', views.PatientViewSet)
router.register('collect', views.CollectViewSet)
router.register('sample', views.SampleViewSet)

# equipment urls
router.register('equipment', views.EquipmentViewSet)
router.register('sensor', views.SensorViewSet)
router.register('sensor/type', views.SensorTypeViewSet)

# history url
router.register('history', views.UseHistoryViewSet)

urlpatterns = [
    path('', include(router.urls))
]
