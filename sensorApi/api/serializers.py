from rest_framework import serializers
from . import models


'''
    Equipment related serizalizers
'''


class EquipmentSerializer(serializers.ModelSerializer):
    """"""

    class Meta:
        model = models.Equipment
        fields = ("__all__")
        read_only_fields = ('id', '_id')


class SensorSerializer(serializers.ModelSerializer):
    """"""

    class Meta:
        model = models.Sensor
        fields = ("__all__")
        read_only_fields = ('id', '_id')


class SensorTypeSerializer(serializers.ModelSerializer):
    """"""
    class Meta:
        model = models.SensorType
        fields = ("__all__")
        read_only_fields = ('id', '_id')


'''
    Patient related serizalizers
'''


class PatientSerializer(serializers.ModelSerializer):
    """"""

    class Meta:
        model = models.Patient
        fields = ("__all__")
        read_only_fields = ('id', '_id')


class SampleSerializer(serializers.ModelSerializer):
    """"""

    class Meta:
        model = models.Sample
        fields = ("__all__")
        read_only_fields = ("__all__")


class CollectSerializer(serializers.ModelSerializer):
    """"""

    class Meta:
        model = models.Collect
        fields = ("__all__")
        read_only_fields = ('id', '_id')


'''
    Patient and equipment related serializers
'''


class UseHistorySerializer(serializers.ModelSerializer):
    """"""

    class Meta:
        model = models.UseHistory
        fields = ("__all__")
        read_only_fields = ('id', '_id')
